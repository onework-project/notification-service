package repo

import (
	"time"
)

type NotificationStorageI interface {
	Create(n *Notification) (int64, error)
	DeleteAll(userId int64) error
	MarkAllAsRead(userId int64) error
	Delete(id int64) error
	Get(id int64) (*Notification, error)
	GetAll(params *GetAllNotificationsParams) (*GetAllNotifications, error)
}

type Notification struct {
	ID          int64
	UserID      int64
	Title       string
	Description string
	ReadAt      time.Time
	Type        string
	MessageID   int64
	CreatedAt   time.Time
}

type GetAllNotificationsParams struct {
	Limit  int64
	Page   int64
	UserID int64
	Type   string
}

type GetAllNotifications struct {
	Notifications []*Notification
	Count         int64
}
