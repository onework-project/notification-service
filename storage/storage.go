package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/notification-service/storage/postgres"
	"gitlab.com/onework-project/notification-service/storage/repo"
)

type StorageI interface {
	Notification() repo.NotificationStorageI
}

type StoragePg struct {
	notificationRepo repo.NotificationStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &StoragePg{
		notificationRepo: postgres.NewNotificationStorage(db),
	}
}

func (s *StoragePg) Notification() repo.NotificationStorageI {
	return s.notificationRepo
}
