package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/notification-service/pkg/utils"
	"gitlab.com/onework-project/notification-service/storage/repo"
)

func createNotification(t *testing.T) *repo.Notification {
	n := &repo.Notification{
		UserID:      utils.RandomInt(),
		Title:       faker.Word(),
		Description: faker.Sentence(),
		Type:        "applicant_status",
		MessageID:   3,
	}
	id, err := dbManager.Notification().Create(n)
	require.NoError(t, err)
	require.NotZero(t, id)
	n.ID = id
	return n
}

func TestCreateNotification(t *testing.T) {
	n := createNotification(t)
	dbManager.Notification().DeleteAll(n.UserID)
}

func TestGetNotification(t *testing.T) {
	n := createNotification(t)
	notification, err := dbManager.Notification().Get(n.ID)
	require.NoError(t, err)
	require.NotZero(t, notification.ID)
	require.NotZero(t, notification.UserID)
	require.NotEmpty(t, notification.Title)
	require.NotEmpty(t, notification.Description)
	require.NotZero(t, notification.CreatedAt)
	require.NotZero(t, notification.MessageID)
	dbManager.Notification().Delete(n.ID)
}

func TestGetAllNotifications(t *testing.T) {
	userId := []int64{}
	for i := 0; i < 10; i++ {
		n := createNotification(t)
		userId = append(userId, n.UserID)
	}
	notifications, err := dbManager.Notification().GetAll(&repo.GetAllNotificationsParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.Equal(t, len(notifications.Notifications), 10)
	for i := 0; i < 10; i++ {
		err = dbManager.Notification().DeleteAll(userId[i])
		require.NoError(t, err)
	}
}

func TestMarkAllAsRead(t *testing.T) {
	for i := 0; i < 10; i++ {
		n := repo.Notification{
			UserID:      1,
			Title:       faker.Word(),
			Description: faker.Sentence(),
			Type:        "application",
			MessageID:   4,
		}
		_, err := dbManager.Notification().Create(&n)
		require.NoError(t, err)
	}

	err := dbManager.Notification().MarkAllAsRead(1)
	require.NoError(t, err)
	notifications, err := dbManager.Notification().GetAll(&repo.GetAllNotificationsParams{
		Limit:  10,
		Page:   1,
		UserID: 1,
	})
	require.NoError(t, err)
	for _, v := range notifications.Notifications {
		require.NotZero(t, v.ReadAt)
	}

	err = dbManager.Notification().DeleteAll(1)
	require.NoError(t, err)
}
