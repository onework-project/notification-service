package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/notification-service/storage/repo"
)

type notificationRepo struct {
	db *sqlx.DB
}

func NewNotificationStorage(db *sqlx.DB) repo.NotificationStorageI {
	return &notificationRepo{
		db: db,
	}
}

func (n *notificationRepo) Create(notification *repo.Notification) (int64, error) {
	query := `
		INSERT INTO notifications (
			user_id,
			title, 
			description,
			type,
			message_id
		) VALUES ($1, $2, $3, $4, $5) 
		RETURNING id
	`
	var id int64
	err := n.db.QueryRow(
		query,
		notification.UserID,
		notification.Title,
		notification.Description,
		notification.Type,
		notification.MessageID,
	).Scan(&id)

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (n *notificationRepo) DeleteAll(userId int64) error {
	query := `
		DELETE FROM notifications WHERE user_id = $1
	`
	res, err := n.db.Exec(query, userId)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (n *notificationRepo) Delete(id int64) error {
	query := `
		DELETE FROM notifications WHERE id = $1
	`
	res, err := n.db.Exec(query, id)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (n *notificationRepo) Get(notificationId int64) (*repo.Notification, error) {
	updateNotification := `
		UPDATE notifications SET read_at = CURRENT_TIMESTAMP WHERE id = $1
	`
	res, err := n.db.Exec(updateNotification, notificationId)
	if err != nil {
		return nil, err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return nil, sql.ErrNoRows
	}

	query := `
		SELECT 
			id,
			user_id,
			title,
			description,
			read_at,
			type,
			message_id,
			created_at
		FROM notifications WHERE id = $1
	`
	var (
		notification repo.Notification
	)
	err = n.db.QueryRow(query, notificationId).Scan(
		&notification.ID,
		&notification.UserID,
		&notification.Title,
		&notification.Description,
		&notification.ReadAt,
		&notification.Type,
		&notification.MessageID,
		&notification.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &notification, nil
}

func (n *notificationRepo) MarkAllAsRead(userID int64) error {
	query := `
		UPDATE notifications SET read_at = CURRENT_TIMESTAMP WHERE user_id = $1 AND read_at IS NULL
	`
	res, err := n.db.Exec(query, userID)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (n *notificationRepo) GetAll(params *repo.GetAllNotificationsParams) (*repo.GetAllNotifications, error) {
	result := repo.GetAllNotifications{
		Notifications: make([]*repo.Notification, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)
	filter := " WHERE true "
	if params.UserID > 0 {
		filter += fmt.Sprintf(" AND user_id = %d ", params.UserID)
	}
	if params.Type != "" {
		filter += fmt.Sprintf(" AND type LIKE '%s' ", params.Type)
	}
	query := `
		SELECT 
			id,
			user_id,
			title, 
			description,
			read_at,
			type,
			message_id,
			created_at
		FROM notifications
	` + filter + `
		ORDER BY created_at DESC
	` + limit
	rows, err := n.db.Query(query)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var (
			notification repo.Notification
			readAt       sql.NullTime
		)
		err := rows.Scan(
			&notification.ID,
			&notification.UserID,
			&notification.Title,
			&notification.Description,
			&readAt,
			&notification.Type,
			&notification.MessageID,
			&notification.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		notification.ReadAt = readAt.Time

		result.Notifications = append(result.Notifications, &notification)
	}

	queryCount := "SELECT count(1) FROM notifications " + filter

	err = n.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
