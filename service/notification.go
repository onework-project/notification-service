package service

import (
	"context"
	"database/sql"
	"errors"
	"strconv"
	"time"

	"firebase.google.com/go/messaging"
	"gitlab.com/onework-project/notification-service/config"
	pb "gitlab.com/onework-project/notification-service/genproto/notification_service"
	emailPkg "gitlab.com/onework-project/notification-service/pkg/email"
	"gitlab.com/onework-project/notification-service/pkg/logging"
	"gitlab.com/onework-project/notification-service/storage"
	"gitlab.com/onework-project/notification-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type NotificationService struct {
	pb.UnimplementedNotificationServiceServer
	cfg       *config.Config
	strg      storage.StorageI
	logging   *logging.Logger
	fcmClient *messaging.Client
}

func NewNotificationService(cfg *config.Config, strg storage.StorageI, logging *logging.Logger, fcmClient *messaging.Client) *NotificationService {
	return &NotificationService{
		cfg:       cfg,
		strg:      strg,
		logging:   logging,
		fcmClient: fcmClient,
	}
}

func (s *NotificationService) SendEmail(ctx context.Context, req *pb.SendEmailRequest) (*emptypb.Empty, error) {
	err := emailPkg.SendEmail(s.cfg, &emailPkg.SendEmailRequest{
		To:      []string{req.To},
		Subject: req.Subject,
		Body:    req.Body,
		Type:    req.Type,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *NotificationService) CreateNotification(ctx context.Context, req *pb.CreateNotificationReq) (*emptypb.Empty, error) {
	notification := repo.Notification{
		UserID:      req.UserId,
		Title:       req.Title,
		Description: req.Description,
		Type:        req.Type,
		MessageID:   req.MessageId,
	}
	_, err := s.strg.Notification().Create(&notification)
	if err != nil {
		s.logging.WithError(err).Error("failed to create notification")
		return nil, status.Errorf(codes.Internal, "failed to create notification: %v", err)
	}
	// TODO: add get image by notification type
	switch req.Type {
	case "applicant_status":
	case "application":
	case "message":
	}
	id := strconv.Itoa(int(req.MessageId))
	// TODO: add image send also
	res, err := s.fcmClient.SendMulticast(context.Background(), &messaging.MulticastMessage{
		Notification: &messaging.Notification{
			Title: req.Title,
			Body:  req.Description,
		},
		Tokens: req.FcmToken,
		Data: map[string]string{
			"notification_type": req.Type,
			"application_id":    id,
		},
	})
	if err != nil {
		s.logging.WithError(err).Error("failed to send notification")
		return nil, status.Errorf(codes.Internal, "failed to send notification: %v", err)
	}
	if res.FailureCount != 0 {
		s.logging.WithError(err).Errorf("failed to send all notification: fail count -> %d", res.FailureCount)
	}
	if res.SuccessCount != len(req.FcmToken) {
		s.logging.WithError(err).Errorf("failed to send all notification: success count -> %d", res.SuccessCount)
	}

	return &emptypb.Empty{}, nil
}

func (s *NotificationService) MarkAllAsRead(ctx context.Context, req *pb.NotificationIdReq) (*emptypb.Empty, error) {
	err := s.strg.Notification().MarkAllAsRead(req.Id)
	if err != nil {
		s.logging.WithError(err).Error("failed to mark all notification as read")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "failed to mark all notification: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "failed to mark all notification as read: %v", err)
	}
	return &emptypb.Empty{}, nil
}

func (s *NotificationService) DeleteNotification(ctx context.Context, req *pb.NotificationIdReq) (*emptypb.Empty, error) {
	err := s.strg.Notification().Delete(req.Id)
	if err != nil {
		s.logging.WithError(err).Error("failed to delete notification")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "failed to delete notification: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "failed to delete notification: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *NotificationService) DeleteAllNotifications(ctx context.Context, req *pb.NotificationIdReq) (*emptypb.Empty, error) {
	err := s.strg.Notification().DeleteAll(req.Id)
	if err != nil {
		s.logging.WithError(err).Error("failed to delete all notifications")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "failed to delete all notifications: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "failed to delete all notifications: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *NotificationService) GetNotification(ctx context.Context, req *pb.NotificationIdReq) (*pb.Notification, error) {
	notification, err := s.strg.Notification().Get(req.Id)
	if err != nil {
		s.logging.WithError(err).Error("failed to get notification: %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "failed to update read_at: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "failed to get notification: %v", err)
	}
	n := pb.Notification{
		Id:          notification.ID,
		UserId:      notification.UserID,
		Title:       notification.Title,
		Description: notification.Description,
		ReadAt:      notification.ReadAt.Format(time.RFC3339),
		Type:        notification.Type,
		MessageId:   notification.MessageID,
		CreatedAt:   notification.CreatedAt.Format(time.RFC3339),
	}
	return &n, nil
}

func (s *NotificationService) GetAllNotifications(ctx context.Context, req *pb.GetAllNotificationsParamsReq) (*pb.GetAllNotificationsRes, error) {
	notifications, err := s.strg.Notification().GetAll(&repo.GetAllNotificationsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		UserID: req.UserId,
		Type:   req.Type,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed to get all notifications: %v", err)
	}
	n := pb.GetAllNotificationsRes{
		Notifications: make([]*pb.Notification, 0),
		Count:         notifications.Count,
	}
	for _, v := range notifications.Notifications {
		i := pb.Notification{
			Id:          v.ID,
			UserId:      v.UserID,
			Title:       v.Title,
			Description: v.Description,
			ReadAt:      v.ReadAt.Format(time.RFC3339),
			Type:        v.Type,
			MessageId:   v.MessageID,
			CreatedAt:   v.CreatedAt.Format(time.RFC3339),
		}
		n.Notifications = append(n.Notifications, &i)
	}

	return &n, nil
}
