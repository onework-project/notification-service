CREATE TABLE IF NOT EXISTS "notifications" (
    "id" serial primary key,
    "user_id" int not null check ("user_id" > 0),
    "title" varchar not null,
    "description" text not null,
    "read_at" timestamp with time zone,
    "type" varchar not null check ("type" in ('applicant_status', 'application', 'message')),
    "message_id" int not null check ("message_id" > 0), 
    "created_at" timestamp with time zone default current_timestamp
);