FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /onework

COPY . .

RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /onework

COPY --from=builder /onework/main .
COPY templates ./templates
COPY onework-3d001-firebase-adminsdk-wcd39-49e7cc0e90.json .

CMD [ "/onework/main" ]