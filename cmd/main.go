package main

import (
	"context"
	"fmt"
	"log"
	"net"

	firebase "firebase.google.com/go"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/onework-project/notification-service/config"
	pb "gitlab.com/onework-project/notification-service/genproto/notification_service"
	"gitlab.com/onework-project/notification-service/pkg/logging"
	"gitlab.com/onework-project/notification-service/service"
	"gitlab.com/onework-project/notification-service/storage"
	"google.golang.org/api/option"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load(".")
	logging.Init()
	logging := logging.GetLogger()
	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}
	opts := []option.ClientOption{option.WithCredentialsFile("./onework-3d001-firebase-adminsdk-wcd39-49e7cc0e90.json")}
	// * initializa firebase app
	app, err := firebase.NewApp(context.Background(), nil, opts...)
	if err != nil {
		logging.WithError(err).Fatal("error in initializing firebase app")
	}

	fcmClient, err := app.Messaging(context.Background())
	if err != nil {
		logging.WithError(err).Fatalf("failed to make client")
	}

	strg := storage.NewStoragePg(psqlConn)

	notificationService := service.NewNotificationService(&cfg, strg, &logging, fcmClient)

	listen, err := net.Listen("tcp", cfg.GrpcPort)

	s := grpc.NewServer()
	pb.RegisterNotificationServiceServer(s, notificationService)
	reflection.Register(s)

	log.Println("gRPC server started port in: ", cfg.GrpcPort)
	if s.Serve(listen); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
